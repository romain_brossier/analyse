#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


CREATE TABLE Etudiant(
        id_etudiant int (11) Auto_increment  NOT NULL ,
        nom         Varchar (50) NOT NULL ,
        prenom      Varchar (50) NOT NULL ,
        date_naiss  Date ,
        num_rue     Integer ,
        nom_rue     Varchar (75) ,
        email       Varchar (100) ,
        cp_ville    Integer ,
        ville       Varchar (75) ,
        photo       Blob ,
        PRIMARY KEY (id_etudiant )
)ENGINE=InnoDB;


CREATE TABLE Promo(
        id_promo      int (11) Auto_increment  NOT NULL ,
        libelle_promo Varchar (75) ,
        nb_eleve      Integer NOT NULL ,
        date_debut    Date NOT NULL ,
        date_fin      Date NOT NULL ,
        id_ville      Int NOT NULL ,
        id_formation  Int NOT NULL ,
        PRIMARY KEY (id_promo )
)ENGINE=InnoDB;


CREATE TABLE Ecf(
        id_ecf     int (11) Auto_increment  NOT NULL ,
        nom        Varchar (50) NOT NULL ,
        descriptif Text ,
        date_ecf   Date NOT NULL ,
        PRIMARY KEY (id_ecf )
)ENGINE=InnoDB;


CREATE TABLE Incident(
        id_incident int (11) Auto_increment  NOT NULL ,
        motif       Varchar (255) ,
        duree       Integer ,
        id_type     Int ,
        PRIMARY KEY (id_incident )
)ENGINE=InnoDB;


CREATE TABLE Type_Incident(
        id_type          int (11) Auto_increment  NOT NULL ,
        libelle_incident Varchar (75) NOT NULL ,
        unite_temps      Varchar (75) NOT NULL ,
        PRIMARY KEY (id_type )
)ENGINE=InnoDB;


CREATE TABLE Ville(
        id_ville  int (11) Auto_increment  NOT NULL ,
        nom_ville Varchar (50) NOT NULL ,
        cp        Integer ,
        PRIMARY KEY (id_ville )
)ENGINE=InnoDB;


CREATE TABLE Type_formation(
        id_formation  int (11) Auto_increment  NOT NULL ,
        nom_formation Varchar (75) NOT NULL ,
        PRIMARY KEY (id_formation )
)ENGINE=InnoDB;


CREATE TABLE concerner(
        id_etudiant Int NOT NULL ,
        id_incident Int NOT NULL ,
        PRIMARY KEY (id_etudiant ,id_incident )
)ENGINE=InnoDB;


CREATE TABLE avoir(
        note            Integer ,
        commentaire_ecf Text NOT NULL ,
        id_ecf          Int NOT NULL ,
        id_promo        Int NOT NULL ,
        id_etudiant     Int NOT NULL ,
        PRIMARY KEY (id_ecf ,id_promo ,id_etudiant )
)ENGINE=InnoDB;


CREATE TABLE appartenir(
        commentaire Text ,
        courant     Bool NOT NULL ,
        id_etudiant Int NOT NULL ,
        id_promo    Int NOT NULL ,
        PRIMARY KEY (id_etudiant ,id_promo )
)ENGINE=InnoDB;

ALTER TABLE Promo ADD CONSTRAINT FK_Promo_id_ville FOREIGN KEY (id_ville) REFERENCES Ville(id_ville);
ALTER TABLE Promo ADD CONSTRAINT FK_Promo_id_formation FOREIGN KEY (id_formation) REFERENCES Type_formation(id_formation);
ALTER TABLE Incident ADD CONSTRAINT FK_Incident_id_type FOREIGN KEY (id_type) REFERENCES Type_Incident(id_type);
ALTER TABLE concerner ADD CONSTRAINT FK_concerner_id_etudiant FOREIGN KEY (id_etudiant) REFERENCES Etudiant(id_etudiant);
ALTER TABLE concerner ADD CONSTRAINT FK_concerner_id_incident FOREIGN KEY (id_incident) REFERENCES Incident(id_incident);
ALTER TABLE avoir ADD CONSTRAINT FK_avoir_id_ecf FOREIGN KEY (id_ecf) REFERENCES Ecf(id_ecf);
ALTER TABLE avoir ADD CONSTRAINT FK_avoir_id_promo FOREIGN KEY (id_promo) REFERENCES Promo(id_promo);
ALTER TABLE avoir ADD CONSTRAINT FK_avoir_id_etudiant FOREIGN KEY (id_etudiant) REFERENCES Etudiant(id_etudiant);
ALTER TABLE appartenir ADD CONSTRAINT FK_appartenir_id_etudiant FOREIGN KEY (id_etudiant) REFERENCES Etudiant(id_etudiant);
ALTER TABLE appartenir ADD CONSTRAINT FK_appartenir_id_promo FOREIGN KEY (id_promo) REFERENCES Promo(id_promo);
