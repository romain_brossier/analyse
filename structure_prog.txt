-- liste des promotions 
SELECT libelle_promo,date_debut,date_fin,nom_ville, count(*) 
FROM promotion
JOIN etudiant_promo on etudiant_promo.id_promo = promotion.id_promo
JOIN villes on promotion.id_ville = villes.id_ville
GROUP BY  libelle_promo,date_debut,date_fin,nom_ville
ORDER BY libelle_promo ;
--ville
SELECT Distinct (nom_ville) FROM ville;
--année
SELECT DISTINCT to_char(date_debut::timestamptz,'YYYY') FROM promotion;
--formation
SELECT DISTINCT nom_formation FROM type_formation;

--nouvelle promotion
Insert into promotion VALUES (nom,dateDeb, dateFin, ville, formation.id);

--supprimer promotion

